import React, { useEffect, useState, useMemo } from "react";
import { FirebaseApp, initializeApp } from "firebase/app";
import { getAuth, signInAnonymously } from "firebase/auth";
import { getDatabase, ref, get, child } from "firebase/database";
import { DB_CATEGORIES } from "constants/index";

interface IAppContextValues {
  fb: FirebaseApp;
  categories: string[];
}

interface IAppContext extends IAppContextValues {
  fb: FirebaseApp;
  categories: string[];
  setCategories: React.Dispatch<React.SetStateAction<string[]>>;
}

export const AppContext = React.createContext<IAppContext>({
  fb: {} as FirebaseApp,
  categories: [],
  setCategories: () => {},
});

const {
  REACT_APP_API_KEY,
  REACT_APP_AUTH_DOMAIN,
  REACT_APP_DATABASE_URL,
  REACT_APP_PROJECT_ID,
  REACT_APP_STORAGE_BUCKET,
  REACT_APP_MESSAGING_SENDER_ID,
  REACT_APP_APP_ID,
} = process.env;

const firebaseConfig = {
  apiKey: REACT_APP_API_KEY,
  authDomain: REACT_APP_AUTH_DOMAIN,
  databaseURL: REACT_APP_DATABASE_URL,
  projectId: REACT_APP_PROJECT_ID,
  storageBucket: REACT_APP_STORAGE_BUCKET,
  messagingSenderId: REACT_APP_MESSAGING_SENDER_ID,
  appId: REACT_APP_APP_ID,
};

const AppContextProvider: React.FC<React.HtmlHTMLAttributes<HTMLDivElement>> = ({ children }) => {
  if (
    !REACT_APP_API_KEY ||
    !REACT_APP_AUTH_DOMAIN ||
    !REACT_APP_DATABASE_URL ||
    !REACT_APP_PROJECT_ID ||
    !REACT_APP_STORAGE_BUCKET ||
    !REACT_APP_MESSAGING_SENDER_ID ||
    !REACT_APP_APP_ID
  ) {
    console.error("process env not set");
  }

  const app = useMemo(() => initializeApp(firebaseConfig), []);

  const db = useMemo(() => getDatabase(app), [app]);

  const [categories, setCategories] = useState<string[]>([]);

  useEffect(() => {
    const auth = getAuth(app);
    signInAnonymously(auth)
      .then((res) => {
        console.log("Auth success: ", res);

        get(child(ref(db), DB_CATEGORIES))
          .then((snapshot) => {
            if (snapshot.exists()) {
              setCategories(snapshot.val());
            } else {
              console.log("No data available");
            }
          })
          .catch((e) => console.log(e));
      })
      .catch((e) => console.log(e));
  }, [app, db]);

  return (
    <AppContext.Provider value={{ fb: app, categories, setCategories }}>
      {React.Children.only(children)}
    </AppContext.Provider>
  );
};

export default React.memo(AppContextProvider);
