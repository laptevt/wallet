import React, { useContext, useMemo, useState, useEffect } from "react";
import cn from "classnames";
import AppContext from "context";
import { getDatabase, ref, onValue } from "firebase/database";
import { DB_DATA } from "constants/index";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import { getSum, excludeCategory } from "utils";
import { SwitchExclude } from "components/SwitchExclude";
import { EXCLUDE_CATEGORY } from "constants/";

import styles from "./TotalExpenses.module.scss";

interface ITotalExpenses extends React.FC<React.HtmlHTMLAttributes<HTMLDivElement>> {
  hideSelect?: boolean;
}

const date = new Date();

const currentDay = date.getDate();
const day = currentDay > 9 ? currentDay : `0${currentDay}`;
const currentMonth = date.getMonth() + 1;
const month = currentMonth > 9 ? currentMonth : `0${currentMonth}`;
const year = date.getFullYear();

const EXPENSES = "Расходы";
const TODAY = "Сегодня";
const MONTH = "Месяц";
const YEAR = "Год";

const TotalExpenses: ITotalExpenses = ({ className }) => {
  const { fb } = useContext(AppContext);

  const db = useMemo(() => getDatabase(fb), [fb]);

  const [sumDay, setSumDay] = useState<number>(0);
  const [sumMonth, setSumMonth] = useState<number>(0);
  const [sumYear, setSumYear] = useState<number>(0);

  const [viewExclude, setViewExclude] = useState(false);

  useEffect(() => {
    onValue(ref(db, `${DB_DATA}/${year}/${month}/${day}`), (snapshot) => {
      if (snapshot.exists()) {
        let data = snapshot.val();

        if (viewExclude) {
          data = excludeCategory(data, EXCLUDE_CATEGORY);
        }

        setSumDay(getSum(data, 1));
      }
    });
  }, [db, viewExclude]);

  useEffect(() => {
    onValue(ref(db, `${DB_DATA}/${year}/${month}`), (snapshot) => {
      if (snapshot.exists()) {
        let data = snapshot.val();

        if (viewExclude) {
          data = excludeCategory(data, EXCLUDE_CATEGORY);
        }

        setSumMonth(getSum(data, 2));
      }
    });
  }, [db, viewExclude]);

  useEffect(() => {
    onValue(ref(db, `${DB_DATA}/${year}`), (snapshot) => {
      if (snapshot.exists()) {
        let data = snapshot.val();

        if (viewExclude) {
          data = excludeCategory(data, EXCLUDE_CATEGORY);
        }

        setSumYear(getSum(data, 3));
      }
    });
  }, [db, viewExclude]);

  return (
    <div className={cn(className, styles.totalExpenses)}>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
        {EXPENSES}
      </Typography>
      <SwitchExclude checked={viewExclude} onChange={(checked) => setViewExclude(checked)} />
      <List className={styles.totalExpenses__list}>
        <ListItem className={cn(styles.totalExpenses__item, styles.totalExpenses__text)}>
          <ListItemText primary={`${TODAY} ${sumDay}`} />
        </ListItem>
        <ListItem className={cn(styles.totalExpenses__item, styles.totalExpenses__text)}>
          <ListItemText primary={`${MONTH} ${sumMonth}`} />
        </ListItem>
        <ListItem className={cn(styles.totalExpenses__item, styles.totalExpenses__text)}>
          <ListItemText primary={`${YEAR} ${sumYear}`} />
        </ListItem>
      </List>
    </div>
  );
};

export default React.memo(TotalExpenses);
