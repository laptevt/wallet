import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

interface IAlertDialog {
  open: boolean;
  title: string;
  text?: string;
  handleClose: (status: boolean) => void;
}

const DISAGREE = "Отменить";
const AGREE = "Подтверждаю";

const AlertDialog: React.FC<IAlertDialog> = ({ open, title, text, handleClose }) => (
  <React.Fragment>
    <Dialog
      open={open}
      onClose={() => handleClose(false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleClose(false)}>{DISAGREE}</Button>
        <Button onClick={() => handleClose(true)} autoFocus>
          {AGREE}
        </Button>
      </DialogActions>
    </Dialog>
  </React.Fragment>
);

export default AlertDialog;
