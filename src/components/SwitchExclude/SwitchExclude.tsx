import React, { FC } from "react";

import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import { EXCLUDE_CATEGORY } from "constants/";

const EXCLUDE = "Исключить категории";

interface ISwitchExcludeProps {
  checked?: boolean;
  onChange: (checked: boolean) => void;
  defaultChecked?: boolean;
}

const SwitchExclude: FC<ISwitchExcludeProps> = ({ checked, onChange, defaultChecked }) => {
  return (
    <FormControlLabel
      control={<Switch defaultChecked={defaultChecked} />}
      value={checked}
      label={EXCLUDE + " " + EXCLUDE_CATEGORY}
      onChange={(_, checked) => onChange(checked)}
    />
  );
};

export default React.memo(SwitchExclude);
