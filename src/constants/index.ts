export const DB_CATEGORIES = process.env.NODE_ENV === "development" ? "categories_test" : "categories";
export const DB_DATA = process.env.NODE_ENV === "development" ? "data_test" : "data";

export const EXCLUDE_CATEGORY = "свадьба";
