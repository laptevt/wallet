export const getSum = (items: Record<string, Record<string, number> | number>, nesting: number = 0): number => {
  let sum = 0;
  Object.values(items).forEach((item) => {
    if (nesting) sum += getSum(item as Record<string, number>, nesting - 1);
    else sum += item as number;
  });

  return sum;
};

export const getCategoriesSum = (
  items: Record<string, Record<string, number> | number>,
  nesting: number = 0,
): Record<string, number> => {
  const obj: Record<string, number> = {};
  for (const [key, value] of Object.entries(items)) {
    if (nesting - 1) {
      const o = getCategoriesSum(value as Record<string, number>, nesting - 1);

      Object.keys(o).forEach((val) => {
        if (obj[val]) obj[val] += o[val];
        else obj[val] = o[val];
      });
    } else {
      if (obj[key]) obj[key] += Object.values(value).reduce((acc, val) => (acc += val), 0);
      else obj[key] = Object.values(value).reduce((acc, val) => (acc += val), 0);
    }
  }

  return obj;
};

export interface IgetCategorieValues {
  date: string[];
  value: number;
  key: string;
}

export const getCategorieValues = (
  items: Record<string, Record<string, number> | number>,
  id: string,
  path: string[],
): IgetCategorieValues[] => {
  if (typeof items === "number") return [];

  let arr: IgetCategorieValues[] = [];

  if (items[id]) {
    for (const [key, value] of Object.entries(items[id])) {
      arr.push({
        date: path,
        value: value as number,
        key,
      });
    }

    return arr;
  }

  for (const [key, value] of Object.entries(items)) {
    arr = [...arr, ...getCategorieValues(value as Record<string, Record<string, number> | number>, id, [...path, key])];
  }

  return arr;
};

export const excludeCategory = (items: Record<string, Record<string, number> | number>, excludeValue: string) => {
  const obj = structuredClone(items);
  Object.entries(items).forEach(([key, value]) => {
    if (key.toLowerCase().includes(excludeValue.toLowerCase())) {
      delete obj[key];
    } else if (typeof value === "object") {
      //@ts-ignore
      obj[key] = excludeCategory(obj[key] as Record<string, number>, excludeValue);
    }
  });

  return obj;
};
