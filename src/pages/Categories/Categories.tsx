import React, { useState, useContext, useMemo } from "react";
import { TextField } from "@mui/material";
import AddBoxIcon from "@mui/icons-material/AddBox";
import cn from "classnames";
import { getDatabase, ref, set } from "firebase/database";
import AppContext from "context";
import styles from "./Categories.module.scss";
import { DB_CATEGORIES } from "constants/index";

const NEW_CATEGORY = "Новая категория";
const EMPTY_CATEGORY = "Категорий нет";

const Categories: React.FC = () => {
  const { fb, categories, setCategories } = useContext(AppContext);

  const db = useMemo(() => getDatabase(fb), [fb]);

  const [value, setValue] = useState("");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const addCategory = () => {
    if (value === "" || categories.includes(value)) return;

    // from db returns value empty if edit value from db
    const arr = [...categories, value.replace("/", "\\")].filter((i) => i);

    set(ref(db, DB_CATEGORIES), arr)
      .then(() => {
        console.log("Категория умпешно добавлена");
        setCategories(arr);
        setValue("");
      })
      .catch((e) => console.log(e));
  };

  return (
    <div className={styles.categories}>
      <div className={styles.categories__list}>
        {categories.length
          ? categories.sort().map((category) => (
              <div className={styles.categories__item} key={category}>
                {category}
              </div>
            ))
          : EMPTY_CATEGORY}
      </div>
      <div className={styles.categories__control}>
        <TextField
          label={NEW_CATEGORY}
          variant="outlined"
          value={value}
          onChange={onChange}
          error={value === ""}
          required
          className={styles.categories__text}
        />
        <AddBoxIcon
          className={cn(styles.categories__icon, {
            [`${styles["categoriesBlock__icon--disable"]}`]: value === "",
          })}
          onClick={addCategory}
        />
      </div>
    </div>
  );
};

export default Categories;
