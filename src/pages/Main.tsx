import React, { useState, useContext, useEffect, useMemo } from "react";
import dayjs, { Dayjs } from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import { Link } from "react-router-dom";
import AppContext from "context";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { getDatabase, ref, set } from "firebase/database";
import { DB_DATA } from "constants/index";
import { v4 as uuidv4 } from "uuid";
import { TotalExpenses } from "components/TotalExpenses";

import styles from "./Main.module.scss";

const DAY = "День";
const CATEGORY = "Категория";
const SUM = "Сумма";
const SEND = "Записать";
const GO_TO_CATEGORY = "Категории";
const GO_TO_TOTAL = "Все расходы";
const DAYJS_FORMAT = "YYYY/MM/DD";

const Main: React.FC = () => {
  const { categories, fb } = useContext(AppContext);

  const db = useMemo(() => getDatabase(fb), [fb]);

  const [date, setDate] = useState<Dayjs>(dayjs(new Date()));

  const changeDate = (value: Dayjs | null) => {
    if (!value) return;

    setDate(value);
  };

  const [category, setCategory] = useState<string>("");

  const changeCategory = (event: SelectChangeEvent) => {
    localStorage.setItem("last_category", event.target.value);
    setCategory(event.target.value);
  };

  useEffect(() => {
    const lastCategory = localStorage.getItem("last_category");

    if (lastCategory && categories.includes(lastCategory)) setCategory(lastCategory);
  }, [categories]);

  const [sum, setSum] = useState<number | null>(null);

  const changeSum = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSum(Number(event.target.value) || null);
  };

  const sendSum = () => {
    set(ref(db, `${DB_DATA}/${dayjs(date).format(DAYJS_FORMAT)}/${category}/${uuidv4()}`), sum)
      .then(() => {
        console.log("Сумма записана");
        setSum(null);
      })
      .catch((e) => {
        console.log("Ошибка записи суммы");
        console.error(e);
      });
  };

  useEffect(() => {
    localStorage.removeItem("total_period");
    localStorage.removeItem("total_date");
  }, []);

  return (
    <div className={styles.main}>
      <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="ru">
        <MobileDatePicker label={DAY} onChange={changeDate} value={date} className={styles.main__input} />
      </LocalizationProvider>

      <div className={styles.main__item}>
        <FormControl fullWidth>
          <InputLabel id="category-label">{CATEGORY}</InputLabel>
          <Select
            labelId="category-labe"
            value={category}
            label="category"
            onChange={changeCategory}
            className={styles.main__input}
          >
            {categories.sort().map((item) => (
              <MenuItem value={item} key={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <div className={styles.main__item}>
        <TextField
          label={SUM}
          type="number"
          value={sum || ""}
          onChange={changeSum}
          fullWidth
          className={styles.main__input}
        />
      </div>

      <div className={styles.main__item}>
        <Button variant="contained" fullWidth disabled={!category || !sum} onClick={sendSum}>
          {SEND}
        </Button>
      </div>

      <Link to="/categories" className={styles.main__item}>
        <Button variant="contained" fullWidth color="warning">
          {GO_TO_CATEGORY}
        </Button>
      </Link>

      <Link to="/total" className={styles.main__item}>
        <Button variant="contained" fullWidth color="success">
          {GO_TO_TOTAL}
        </Button>
      </Link>

      <TotalExpenses className={styles.main__item} />
    </div>
  );
};

export default Main;
