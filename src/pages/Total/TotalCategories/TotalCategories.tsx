import { useContext, useMemo, useEffect, useState, useCallback } from "react";
import { useParams, useLocation } from "react-router-dom";
import AppContext from "context";
import { getDatabase, ref, get, child, remove } from "firebase/database";
import { getCategorieValues, IgetCategorieValues } from "utils";
import Typography from "@mui/material/Typography";
import styles from "./TotalCategories.module.scss";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import DeleteIcon from "@mui/icons-material/Delete";
import { DB_DATA } from "constants/index";
import { AlertDialog } from "components/AlertDialog";

const TOTAL_CATEGORY = "Траты по категории";
const LIST_EMPTY = "Нет данных";
const ALERT_QUESTION = "Вы уверены что хотите удалить значение?";

interface IRemoveData {
  date: string[];
  key: string;
  index: number;
}

const TotalCategories = () => {
  const { id } = useParams();
  const location = useLocation();

  const path = new URLSearchParams(location.search).get("path");

  const { fb } = useContext(AppContext);

  const db = useMemo(() => getDatabase(fb), [fb]);

  const [list, setList] = useState<IgetCategorieValues[]>([]);

  const [alertIsOpen, setAlertIsOpen] = useState<boolean>(false);

  const [removeData, setRemoveData] = useState<IRemoveData | null>(null);

  useEffect(() => {
    if (!path || !id) return;

    get(child(ref(db), path))
      .then((snapshot) => {
        if (snapshot.exists()) {
          const _path = path.split("/");
          _path.shift();
          setList(getCategorieValues(snapshot.val() as Record<string, Record<string, number> | number>, id, _path));
        } else {
          console.log("No data available");
        }
      })
      .catch((e) => console.log(e));
  }, [db, path, id]);

  const removeItem = useCallback((date: string[], key: string, index: number) => {
    setRemoveData({
      date,
      key,
      index,
    });
    setAlertIsOpen(true);
  }, []);

  const handleClose = useCallback(
    (status: boolean) => {
      setAlertIsOpen(false);
      if (status && removeData) {
        remove(ref(db, `${DB_DATA}/${removeData.date.join("/")}/${id}/${removeData.key}`))
          .then(() => {
            setList((_prev) => {
              const prev = [..._prev];
              prev.splice(removeData.index, 1);
              return prev;
            });
            console.log("Запись успешно удалена");
          })
          .catch((e) => {
            console.error("Ошибка удаления записи: ", e);
          });
      }
    },
    [db, id, removeData],
  );

  if (!id || !path) return <div>Query параметр не передан</div>;

  return (
    <div className={styles.totalCategories}>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
        {`${TOTAL_CATEGORY}: `}
        <span className={styles.totalCategories__category}>{id}</span>
      </Typography>
      {!list.length && LIST_EMPTY}
      <List className={styles.totalCategories__list}>
        {list.map(({ date, value, key }, index) => (
          <ListItem key={key} className={styles.totalCategories__item}>
            <ListItemText className={styles.totalCategories__text}>
              {`${[...date].reverse().join(".")}`}
              <span>{value}</span>
              <DeleteIcon onClick={() => removeItem(date, key, index)} />
            </ListItemText>
          </ListItem>
        ))}
      </List>
      <AlertDialog open={alertIsOpen} title={ALERT_QUESTION} handleClose={handleClose} />
    </div>
  );
};

export default TotalCategories;
