import { useState, useMemo, useContext, useEffect } from "react";
import dayjs, { Dayjs } from "dayjs";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import { DateView } from "@mui/x-date-pickers/models";
import AppContext from "context";
import { getDatabase, ref, get, child } from "firebase/database";
import { DB_DATA, EXCLUDE_CATEGORY } from "constants/index";
import { getCategoriesSum, getSum, excludeCategory } from "utils";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import cn from "classnames";
import { Link } from "react-router-dom";
import { SwitchExclude } from "components/SwitchExclude";

import styles from "./Total.module.scss";

enum TIME {
  DAY = "Day",
  MONTH = "Month",
  YEAR = "Year",
}

const PERIOD = "Период";
const DATE = "Дата";
const EMPTY = "Нет данных за данный период";
const TOTAL_SUM = "Всего";

const YEAR_FORMAT = "YYYY";
const MONTH_FORMAT = "YYYY/MM";
const DAY_FORMAT = "YYYY/MM/DD";

const Total = () => {
  const { fb } = useContext(AppContext);

  const db = useMemo(() => getDatabase(fb), [fb]);

  const localPeriod = localStorage.getItem("total_period") as TIME | undefined;
  const localDate = localStorage.getItem("total_date");

  const [period, setPeriod] = useState<TIME>(localPeriod || TIME.DAY);

  const changePeriod = (event: SelectChangeEvent) => {
    setPeriod(event.target.value as TIME);
    localStorage.setItem("total_period", event.target.value);
  };

  const [date, setDate] = useState<Dayjs>(dayjs(localDate || new Date()));

  const changeDate = (value: Dayjs | null) => {
    if (!value) return;

    setDate(value);
    localStorage.setItem("total_date", dayjs(value).format(DAY_FORMAT));
  };

  const views = useMemo(() => {
    switch (period) {
      case TIME.YEAR:
        return [TIME.YEAR.toLowerCase()];
      case TIME.MONTH:
        return [TIME.MONTH.toLowerCase(), TIME.YEAR.toLowerCase()];
      default:
        return [TIME.YEAR.toLowerCase(), TIME.MONTH.toLowerCase(), TIME.DAY.toLowerCase()];
    }
  }, [period]);

  const [list, setList] = useState<Record<string, number>>({});
  const [totalSum, setTotalSum] = useState<number>(0);
  const [path, setPath] = useState<string>("");

  const [viewExclude, setViewExclude] = useState(true);

  const listFormat = useMemo(() => {
    return Object.entries(list).sort((a, b) => b[1] - a[1]);
  }, [list]);

  useEffect(() => {
    let path = "";
    let nesting = 0;
    switch (period) {
      case TIME.YEAR:
        path = dayjs(date).format(YEAR_FORMAT);
        nesting = 3;
        break;
      case TIME.MONTH:
        path = dayjs(date).format(MONTH_FORMAT);
        nesting = 2;
        break;
      default:
        path = dayjs(date).format(DAY_FORMAT);
        nesting = 1;
    }

    setPath(`${DB_DATA}/${path}`);

    get(child(ref(db), `${DB_DATA}/${path}`))
      .then((snapshot) => {
        if (snapshot.exists()) {
          let data = snapshot.val();

          if (viewExclude) {
            data = excludeCategory(data, EXCLUDE_CATEGORY);
          }
          setTotalSum(getSum(data, nesting));
          setList(getCategoriesSum(data, nesting));
        } else {
          setTotalSum(0);
          setList({});
          console.log("No data available");
        }
      })
      .catch((e) => console.log(e));
  }, [date, period, db, viewExclude]);

  return (
    <div className={styles.total}>
      <div className={styles.total__col}>
        <FormControl fullWidth>
          <InputLabel id="total-label">{PERIOD}</InputLabel>
          <Select
            labelId="total-label"
            value={period}
            label="total"
            onChange={changePeriod}
            className={styles.total__control}
          >
            {Object.values(TIME).map((item) => (
              <MenuItem value={item} key={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <div className={styles.total__col}>
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="ru">
          <MobileDatePicker
            label={DATE}
            onChange={changeDate}
            value={date}
            className={cn(styles.total__calendar, styles.total__control)}
            views={views as DateView[]}
          />
        </LocalizationProvider>
      </div>
      <SwitchExclude checked={viewExclude} onChange={(checked) => setViewExclude(checked)} defaultChecked={true} />
      <div className={styles.total__col}>
        <List className={styles.total__list}>
          {totalSum ? (
            <>
              <ListItem className={styles.total__item}>
                <ListItemText primary={`${TOTAL_SUM} ${totalSum}`} />
              </ListItem>
              {listFormat.map(([key, value]) => (
                <ListItem className={styles.total__item} key={key}>
                  <Link
                    to={{
                      pathname: `/total/${key}`,
                      search: `path=${path}`,
                    }}
                    className={styles.total__link}
                  >
                    <ListItemText primary={`${key} ${value}`} />
                  </Link>
                </ListItem>
              ))}
            </>
          ) : (
            <ListItem className={styles.total__item}>
              <ListItemText primary={EMPTY} />
            </ListItem>
          )}
        </List>
      </div>
    </div>
  );
};

export default Total;
