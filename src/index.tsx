import "dayjs/locale/ru";

import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, Outlet, createHashRouter } from "react-router-dom";
import Main from "./pages/Main";
import Categories from "./pages/Categories/Categories";
import Total from "./pages/Total/Total";
import TotalCategories from "./pages/Total/TotalCategories/TotalCategories";
import { AppContextProvider } from "./context";

import "./index.css";

const Router = () => {
  const router = createHashRouter([
    {
      path: "/",
      element: <Outlet />,
      children: [
        {
          index: true,
          element: <Main />,
        },
        {
          path: "categories",
          element: <Categories />,
        },
        {
          path: "total",
          element: <Total />,
        },
        {
          path: "total/:id",
          element: <TotalCategories />,
        },
      ],
    },
  ]);

  return (
    <AppContextProvider>
      <RouterProvider router={router} />
    </AppContextProvider>
  );
};

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>,
);
